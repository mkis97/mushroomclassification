import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sb
from pandas.plotting import scatter_matrix
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, roc_curve, auc
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.externals import joblib


def measurePerformance(y_test,y_pred):
    false_positive_rate, true_positive_rate, thresholds = roc_curve(y_test, y_pred)
    print("Threshold: ", thresholds)
    roc_auc = auc(false_positive_rate, true_positive_rate)
    return false_positive_rate, true_positive_rate, roc_auc
  


def evaluateClassificationModel(confussionMatrix):
    TP=float(confussionMatrix[0,0]) #true poisonous mushroom
    TN=float(confussionMatrix[1,1]) #true edible mushroom
    FP=float(confussionMatrix[0,1]) #false poisonous mushroom
    FN=float(confussionMatrix[1,0]) #true edible mushroom
    acc=(TP+TN)/(TP+TN+FP+FN)*100 #Accuracy
    print("Accuracy: " + str(round(acc,3)) + "%")
    miss=(100-acc) #Missclassification rate
    print("Missclassification rate: " + str(round(miss,3)) + "%")
    prec=TP/(TP+FP)*100 #Precision
    print("Precision: " + str(round(prec,3)) + "%")
    re=TP/(TP+FN)*100 #Recall
    print("Recall: " + str(round(re,3)) + "%")
    spec=TN/(TN+FP)*100 #Specificity
    print("Specificity: " + str(round(spec,3)) + "%")
    err=FN/(TP+FN)*100 #Type II error
    print("Type II error: " + str(round(err,3)) + "%")

#importing mushrooms dataset
data=pd.read_csv('mushrooms.csv')

#get info about data
data.info()

#visualization of edible/posionous mushrooms
data['class'].value_counts().sort_index().plot.bar(color=('green', 'red'))
plt.xlabel("Class")
plt.ylabel("Count")
plt.title("Class count")
plt.show()

#droping veil-type column because it has only one value
data.drop("veil-type",axis=1,inplace=True)

#added to control memory consumption warning
plt.rcParams.update({'figure.max_open_warning': 0})

#function for plotting
def plotData(mainClass, data):
    for ind, column in enumerate(data.columns):
        plt.figure(ind)
        sb.countplot(x=data[column], hue=mainClass, data=data)

mainVisual=data['class']
dataForVisual=data.drop('class', 1)
plotData(mainVisual,dataForVisual)
plt.show()

#converting columns to numeric values
data["class"] = [0 if item == "e" else 1 for item in data["class"]]

for column in data.drop(["class"], axis=1).columns:
    value = 0
    step = 1/(len(data[column].unique())-1)
    for i in data[column].unique():
        data[column] = [value if letter == i else letter for letter in data[column]]
        value += step

#data histogram
data.hist()
plt.show()



#preparing data 
y = data["class"].values
x = data.drop(["class"], axis=1).values

x_list=list(data.drop(["class"], axis=1).columns)

#splitting training and testing data 70%-30%
x_train, x_test, y_train, y_test = train_test_split(x,y,random_state=0,test_size=0.3)


#Logistic Regression
logReg = LogisticRegression()
logReg.fit(x_train,y_train)
print("Logistic regression:")
y_predLogReg = logReg.predict(x_test)
confusion_matrix_logReg = confusion_matrix(y_test, y_predLogReg)
print(confusion_matrix_logReg)
measurePerformance(y_test,y_predLogReg)
evaluateClassificationModel(confusion_matrix_logReg)
sb.heatmap(confusion_matrix_logReg, annot=True, fmt = ".0f",)
plt.title("Logistic regression confusion matrix")
plt.show()


#Gaussian Naive Bayes
gnb=GaussianNB()
gnb.fit(x_train,y_train)
print("Gaussian naive Bayes:")
y_predGaussianNB=gnb.predict(x_test)
confusion_matrix_gaussianNB = confusion_matrix(y_test, y_predGaussianNB)
evaluateClassificationModel(confusion_matrix_gaussianNB)
print(confusion_matrix_gaussianNB)
measurePerformance(y_test,y_predGaussianNB)
sb.heatmap(confusion_matrix_gaussianNB, annot=True, fmt = ".0f",)
plt.title("Gaussian naive Bayes confusion matrix")
plt.show()


#KNN
best_Kvalue = 0
best_score = 0
for i in range(1,10):
    knn = KNeighborsClassifier(n_neighbors=i)
    knn.fit(x_train,y_train)
    if knn.score(x_test,y_test) > best_score:
        best_score = knn.score(x_train,y_train)
        best_Kvalue = i
print("""Best KNN Value: {}""".format(best_Kvalue))
knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(x_train,y_train)
y_predKnn = knn.predict(x_test)
confusion_matrix_knn = confusion_matrix(y_test, y_predKnn)
evaluateClassificationModel(confusion_matrix_knn)
print(confusion_matrix_knn)
measurePerformance(y_test,y_predKnn)
sb.heatmap(confusion_matrix_knn, annot=True, fmt = ".0f",)
plt.title("KNN confusion matrix")
plt.show()


#SVM
svm = SVC(random_state=42, gamma="auto")
svm.fit(x_train,y_train)
print("SVM:")
y_predSvm=svm.predict(x_test)
confusion_matrix_svm = confusion_matrix(y_test, y_predSvm)
evaluateClassificationModel(confusion_matrix_svm)
print(confusion_matrix_svm)
measurePerformance(y_test,y_predSvm)
sb.heatmap(confusion_matrix_svm, annot=True, fmt = ".0f",)
plt.title("SVM confusion matrix")
plt.show()


#Decision Tree
dt = DecisionTreeClassifier()
dt.fit(x_train,y_train)
print("Decision tree:")
y_predGaussianDecisionTree=dt.predict(x_test)
confusion_matrix_decisionTree = confusion_matrix(y_test, y_predGaussianDecisionTree)
evaluateClassificationModel(confusion_matrix_decisionTree)
print(confusion_matrix_decisionTree)
measurePerformance(y_test,y_predGaussianDecisionTree)
sb.heatmap(confusion_matrix_decisionTree, annot=True, fmt = ".0f",)
plt.title("Decision tree confusion matrix")
plt.figure(figsize=[12.8,9.6])
plot_tree(dt, class_names=True, fontsize="7", filled=True, feature_names=x_list)
plt.show()


#Random Forest
rf = RandomForestClassifier(random_state=0)
rf.fit(x_train,y_train)
joblib.dump(rf, 'model.pkl')
print("Random forest:")
y_predRandomForest=rf.predict(x_test)
confusion_matrix_randomForest = confusion_matrix(y_test, y_predRandomForest)
evaluateClassificationModel(confusion_matrix_randomForest)
print(confusion_matrix_randomForest)
measurePerformance(y_test,y_predRandomForest)
sb.heatmap(confusion_matrix_randomForest, annot=True, fmt = ".0f",)
plt.title("Random forest confusion matrix")
plt.show()



#ROC - AUC comparsion
logReg_false_positive_rate, logReg_true_positive_rate, logReg_roc_auc = measurePerformance(y_test,y_predLogReg)
gnb_false_positive_rate, gnb_true_positive_rate, gnb_roc_auc=measurePerformance(y_test,y_predGaussianNB)
knn_false_positive_rate, knn_true_positive_rate, knn_roc_auc=measurePerformance(y_test,y_predKnn)
svm_false_positive_rate,svm_true_positive_rate,svm_roc_auc=measurePerformance(y_test,y_predSvm)
dt_false_positive_rate,dt_true_positive_rate,dt_roc_auc=measurePerformance(y_test,y_predGaussianDecisionTree)
rf_false_positive_rate,rf_true_positive_rate,rf_roc_auc=measurePerformance(y_test,y_predRandomForest)
plt.plot(logReg_false_positive_rate, logReg_true_positive_rate, color='red',label = 'Logistic Regression AUC = %0.3f' % logReg_roc_auc)
plt.plot(gnb_false_positive_rate, gnb_true_positive_rate, color='green',label = 'Gaussian Naive Bayes AUC = %0.3f' % gnb_roc_auc)
plt.plot(knn_false_positive_rate,knn_true_positive_rate, color='blue',label = 'KNN AUC = %0.3f' % knn_roc_auc)
plt.plot(svm_false_positive_rate,svm_true_positive_rate, color='orange',label = 'SVM AUC = %0.3f' % svm_roc_auc)
plt.plot(dt_false_positive_rate,dt_true_positive_rate, color='purple',label = 'Decison Tree AUC = %0.3f' % dt_roc_auc)
plt.plot(rf_false_positive_rate,rf_true_positive_rate, color='brown',label = 'Random Forest AUC = %0.3f' % rf_roc_auc)
plt.legend(loc = 'lower right')
plt.plot([0, 1], [0, 1],linestyle='--', color='black')
plt.axis('tight')
plt.ylabel('True Positive Rate')
plt.xlabel('False Positive Rate')
plt.show()